// config type params
type ParamsProps = {
    key :string;
    value :string;
    defaultValue :boolean;
}